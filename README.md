# README #

Expand Coupon widget in cart and checkout

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/DiscountCollapsillable when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

Add product to the cart and check the cart render a discount widget that is expanded by default.
Go to the checkout and the same applies.

For both areas, the discount widget is still fully javascript powered as per default Luma behaviour.
--> for instance, you can collapse the discount and expand it again.